import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Solicitacao } from 'src/app/model/Solicitacao';
import { DialogData } from '../criar-roteiro/criar-roteiro-dialog.component';

@Component({
    selector: 'app-detalhes-solicitacao-dialog',
    templateUrl: 'detalhes-solicitacao.component.html'
})
export class DetalhesSolicitacaoDialogComponent {

    solicitacao: Solicitacao;

    constructor(
        private dialogRef: MatDialogRef<DetalhesSolicitacaoDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: DialogData,
        private dialog: MatDialog) { }

    ngOnInit() {
        this.solicitacao = this.data.solicitacao;
    }

    fechar() {
        this.dialogRef.close();
    }

    getCategoria(categoria: number): string {
        switch (categoria) {
          case 0:
            return 'Iluminação pública';
          case 1:
            return 'Infração de trânsito';
          case 2:
            return 'Má conservação de patrimônio';
          case 3:
            return 'Outros';
        }
      }
}