import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciaRoteiroComponent } from './gerencia-roteiro.component';

describe('GerenciaSolicitacaoComponent', () => {
  let component: GerenciaRoteiroComponent;
  let fixture: ComponentFixture<GerenciaRoteiroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciaRoteiroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciaRoteiroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
