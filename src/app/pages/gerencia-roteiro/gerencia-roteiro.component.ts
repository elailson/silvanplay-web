import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ToasterService } from 'angular2-toaster';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatPaginator } from '@angular/material';
import { Roteiro } from 'src/app/model/Roteiro';
import { RoteiroService } from 'src/app/services/roteiro.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { CriarRoteiroDialogComponent } from 'src/app/dialogs/criar-roteiro/criar-roteiro-dialog.component';

@Component({
  selector: 'app-gerencia-roteiro',
  templateUrl: './gerencia-roteiro.component.html',
  styleUrls: ['./gerencia-roteiro.component.css']
})
export class GerenciaRoteiroComponent implements OnInit {

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @BlockUI() blockUI: NgBlockUI;
  dataSource: MatTableDataSource<Roteiro>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = ['roteiro', 'nome', 'campus', 'turno', 'horario', 'assentosDisp'];

  pesquisaRoteiro = this.formBuilder.group({
    nome: new FormControl(''),
    campus: new FormControl(''),
    roteiro: new FormControl('')
  });

  constructor(private roteiroService: RoteiroService,
    private toasterService: ToasterService,
    private formBuilder: FormBuilder,
    private createDialog: MatDialog) { }

  ngOnInit() {
    this.blockUI.start('Buscando roteiros');
    this.roteiroService.findAll()
      .subscribe(roteiros => {
        this.dataSource = new MatTableDataSource(roteiros);
        this.dataSource.paginator = this.paginator;
        this.blockUI.stop();
      }, error => {
        this.toasterService.pop('warning', 'Erro ao buscar roteiros')
        this.blockUI.stop();
      });
  }

  openDialog() {
    this.createDialog.open(CriarRoteiroDialogComponent, {
      width: '900px'
    });
  }

  search() {
    let nome = this.isValid(this.pesquisaRoteiro.get('nome').value);
    let campus = this.isValid(this.pesquisaRoteiro.get('campus').value);
    let roteiro = this.isValid(this.pesquisaRoteiro.get('roteiro').value);
    this.blockUI.start('Pesquisando roteiros...');
    this.roteiroService.search(nome, campus, roteiro)
      .subscribe(roteiros => {
        this.dataSource = new MatTableDataSource(roteiros);
        this.dataSource.paginator = this.paginator;
        this.blockUI.stop();
      });
  }

  isValid(input: string): string {
    return input.trim().length === 0 ? null : input;
  }

}
