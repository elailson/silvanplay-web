import { Component, OnInit } from '@angular/core';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ToasterService } from 'angular2-toaster';
import { Administrador } from 'src/app/model/Administrador';
import { AuthenticationService } from 'src/app/auth/services/authentication.service';
import { LocalStorageService } from 'angular-web-storage';
import { Router } from '@angular/router';
import { HeaderComponent } from '../template/header/header.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;
  loginForm: FormGroup;
  login: Administrador;

  constructor(
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private toasterService: ToasterService,
    private localStorage: LocalStorageService,
    private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      usuario: ['', Validators.required],
      senha: ['', Validators.required]
    });
  }

  autentica() {
    if (this.validarLogin()) {
      this.blockUI.start('Autenticando');
      this.authService.autentica(this.login).subscribe(retorno => {
        if (retorno) {
          this.localStorage.set('usuario', retorno.usuario);
          this.localStorage.set('tipoToken', retorno.token.substring(0, 6));
          this.localStorage.set('token', retorno.token.substr(6));
          this.verificaPerfil(retorno);
        } else {
          this.toasterService.pop('error', 'Usuário ou senha inválidos');
        }
        this.blockUI.stop();
      }, error => {
        this.toasterService.pop('error', 'Erro de comunicação com o servidor: ' + error.statusText);
        this.blockUI.stop();
      })
    }
  }

  validarLogin(): boolean {
    if (this.loginForm.valid) {
      this.login = new Administrador(this.loginForm.get('usuario').value, this.loginForm.get('senha').value);
      return true;
    } else {
      return false;
    }
  }

  verificaPerfil(login: Administrador) {
    const permissoes = [];
    login.permissao.split(',').forEach(permissao => {
      permissoes.push(permissao);
    });

    this.localStorage.set('permissoes', permissoes);

    this.router.navigate(['home'])
      .then(() => window.location.reload());
  }

}
