import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { AdminEnum } from 'src/app/enum/AdminEnum';

export class AuthGuardSolicitacao implements CanActivate {

    constructor(private router: Router,
        private localStorageService: LocalStorageService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let retorno = false;

        if (this.isLogado()) {
            const permissoes = this.localStorageService.get('permissoes');

            if (permissoes) {
                permissoes.forEach((permissao: string) => {
                    if (permissao.trim() === AdminEnum.GERENCIA_SOLICITACAO) {
                        retorno = true;
                    }
                });
                return retorno;
            }
            this.router.navigate(['home']);
        }

        return retorno;
    }

    isLogado(): boolean {
        const usuario = this.localStorageService.get('usuario');
        if (!usuario) {
            this.router.navigate(['login']);
            return false;
        }

        return true;
    }
}