export class Solicitacao {

    idSolicitacao: number;
    cidade: string;
    bairro: string;
    rua: string;
    complemento: string;
    latitude: number;
    longitude: number;
    caminhoImagem: string;
    categoria: number;
    descricao: string;
    status: number;
    notificado: number;
    resposta: string;
    retornoUsuario: number;
    idUsuario: string;
}