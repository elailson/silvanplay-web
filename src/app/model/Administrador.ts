export class Administrador {

    id: number;
    usuario: string;
    senha: string;
    permissao: string;
    tipoToken: string;
    token: string;
    permissaoString: string;

    constructor(usuario: string, senha: string) {
        this.usuario = usuario;
        this.senha = senha;
    }
}