import { FormGroup } from '@angular/forms';

export class Instituicao {
    
    idColegio: number;
    nome: string;
    bairro: string;
    rua: string;
    numero: number;
    telefone: string;
    diretor: string;
    segmento: string;
    turno: string;
    integral: number;
    multiFuncional: number;

    constructor(instituicao: FormGroup) {
        this.nome = instituicao.get('nome').value.toLocaleUpperCase();
        this.bairro = instituicao.get('bairro').value.toLocaleUpperCase();
        this.rua = instituicao.get('rua').value.toLocaleUpperCase();
        this.numero = instituicao.get('numero').value;
        this.telefone = instituicao.get('telefone').value;
        this.diretor = instituicao.get('diretor').value.toLocaleUpperCase();
        this.segmento = JSON.stringify(instituicao.get('segmento').value).replace(/[\[\]']+/g, '');
        this.turno = JSON.stringify(instituicao.get('turno').value).replace(/[\[\]']+/g, '');
        this.integral = instituicao.get('integral').value;
        this.multiFuncional = instituicao.get('multiFuncional').value;
    }
}