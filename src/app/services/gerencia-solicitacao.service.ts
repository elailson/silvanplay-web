import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { environmentProd } from '../../environments/environment.prod';
import { Solicitacao } from '../model/Solicitacao';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class GerenciaSolicitacaoService {

    private relativePath: string = environment.url;
    // private relativePath: string = environmentProd.url;

    constructor(private http: HttpClient) { }

    findAll(): Observable<Array<Solicitacao>> {
        return this.http.get<Array<Solicitacao>>(this.relativePath + 'solicitacao/find-all');
    }

    protected getHeaders() {
        const headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        return headers;
    }
}