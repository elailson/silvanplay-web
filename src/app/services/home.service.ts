import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Home } from '../model/Home';

@Injectable({ providedIn: 'root' })
export class HomeService {

    private relativePath: string = environment.url;

    constructor(private http: HttpClient) { }

    buscarDados(): Observable<Home> {
        return this.http.get<Home>(this.relativePath + 'home/dados');
    }
}